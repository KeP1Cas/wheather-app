module.exports = {
  globDirectory: "build/",
  globPatterns: ["**/*.{json,png,html,css,js,txt,svg,webmanifest}"],
  ignoreURLParametersMatching: [/^utm_/, /^fbclid$/],
  swDest: "build/sw.js",
};
