import React, { useState, useEffect } from "react";

import { OfflineMode, OfflineModeIcon } from "./styled";

import OfflineIcon from "../../assets/icons/no-signal.png";
const OnlineMode = () => {
  const [onlineMode, setOnlineMode] = useState(true);

  function handleNetworkChange(): void {
    if (navigator.onLine) {
      setOnlineMode;
    } else {
      setOnlineMode(false);
    }
  }

  useEffect(() => {
    handleNetworkChange();
    window.addEventListener("online", handleNetworkChange);
    window.addEventListener("offline", handleNetworkChange);
  }, []);

  return (
    <OfflineMode>
      {!onlineMode && <OfflineModeIcon src={OfflineIcon} alt="No Signal" />}
    </OfflineMode>
  );
};

export { OnlineMode };
