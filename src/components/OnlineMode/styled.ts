import styled from "styled-components";

const OfflineMode = styled.div`
  position: fixed;
  right: 20px;
  top: 20px;
  z-index: 3;
`;

const OfflineModeIcon = styled.img`
  width: 30px;
  height: 30px;
`;

export { OfflineMode, OfflineModeIcon };
