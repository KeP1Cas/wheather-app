import React from "react";

import { AppBackground } from "../layouts/AppBackground";
import {
  AppTitleMainContainer,
  WeatherContainersContainer,
  ILoveMercuryContainer,
} from "./styled";
import { AppTitle } from "../layouts/AppTitle";
import { WeatherContainer } from "../container/WeatherContainer";
import { Meta } from "../elements/Meta";
import { OnlineMode } from "../components/OnlineMode";

const App: React.FC = () => {
  return (
    <AppBackground>
      <AppTitleMainContainer>
        <AppTitle />
        <OnlineMode />
      </AppTitleMainContainer>
      <WeatherContainersContainer>
        <WeatherContainer isSevenDaysCard />
        <WeatherContainer />
      </WeatherContainersContainer>
      <ILoveMercuryContainer>
        <Meta uppercase>C ЛЮБОВЬЮ К ПОГОДЕ</Meta>
      </ILoveMercuryContainer>
    </AppBackground>
  );
};

export { App };
